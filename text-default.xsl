<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text"/>
  <xsl:template match="/newsletter">
    <xsl:value-of select="title"/><xsl:text>&#13;</xsl:text>
    <xsl:value-of select="link"/><xsl:text>&#13;&#13;</xsl:text>
    <xsl:if test="date">
      <xsl:value-of select="date"/><xsl:text>&#13;&#13;</xsl:text>
    </xsl:if>
    <xsl:if test="message">
      <xsl:value-of select="message"/><xsl:text>&#13;&#13;</xsl:text>
    </xsl:if>
    <xsl:for-each select="section">
      <xsl:if test="name">
        <xsl:value-of select="name"/>
        <xsl:text>&#13;--------------------------------&#13;&#13;</xsl:text>
      </xsl:if>
      <xsl:for-each select="content"> 
        <xsl:value-of select="title"/><xsl:text>&#13;</xsl:text>
        <xsl:value-of select="link"/><xsl:text>&#13;</xsl:text>
        <xsl:if test="author">
          <xsl:value-of select="author"/><xsl:text>&#13;</xsl:text>
          <xsl:value-of select="authorlink"/><xsl:text>&#13;</xsl:text>
        </xsl:if>
        <xsl:value-of select="date"/><xsl:text>&#13;</xsl:text>
        <xsl:value-of select="teaser"/><xsl:text>&#13;&#13;</xsl:text>
      </xsl:for-each>
    </xsl:for-each>
    <xsl:value-of select="notice"/>
  </xsl:template>
</xsl:stylesheet>
