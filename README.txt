
Welcome to SunMailer!

This will guide you through installing, configuring, and using SunMailer.  For
more detailed help, install the Advanced Help module.  Neither this README nor
the advanced help will explain how to set up your server as a mail server; you
will have to do that yourself or have your hosting company do it for you.

I.  Dependencies
  A.  XSL extension
  B.  PHPMailer
  C.  Cron (optional)
II.  Configuration
  A.  SunMailer
  B.  PHPMailer
III.  Newsletter
  A.  Configuring
  B.  Sending

I. Dependencies

A. XSL extension

SunMailer requires PHP's XSL extension.  Installing the XSL extension is similar
to installing PHP's GD extension, which Drupal provides some help for at:

http://drupal.org/node/256876

The exact details for how to do this depend on your operating system and/or your
hosting company.  PHP's documentation and Bing/Google may also be useful here.

B. PHPMailer

PHPMailer is a third-party library used by SunMailer to send emails.  Many Linux
distributions provide it through a package.  If not, you can download it at:

http://phpmailer.worxware.com

Download the regular version, not an alternate version such as PHPMailer-FE.
Extract the code to a reasonable location onto your server (e.g.: Ubuntu's
libphp-phpmailer creates the directory /usr/share/php/libphp-phpmailer and puts
the code there).  If you can only access your Drupal installation and not the
entire server, put the code somewhere in the sites folder of your Drupal
installation.  You will need to configure PHPMailer later (see II.B.).

C. Cron (optional)

SunMailer can schedule the newsletter to be sent automatically.  This feature
requires cron.  Drupal provides a good guide to set up cron at:

http://drupal.org/getting-started/6/install/cron

II. Configuration

A.  SunMailer

SunMailer has several permissions.  Assign them to the appropriate roles at:

Administer > User management > Permissions

B.  PHPMailer

Before you can use SunMailer, you will need to configure PHPMailer.  Go to:

Administer > Site configuration > SunMailer > Configure SunMailer > PHPMailer settings

If PHPMailer is in PHP's include path, leave the PHPMailer path field blank.
Otherwise, provide the directory to PHPMailer's directory.  On Linux, this
command can help you find this directory:

locate class.phpmailer.php

Fill in the remaining required fields and click Submit to save these changes.
Also, if you want to send a test email, fill in the Test email field with an
email address and click the Send test email button instead.

III.  Newsletter

A.  Configuring

The next step is configure the content of the newsletter.  Start by going to:

Administer > Site configuration > SunMailer > Configure SunMailer > Newsletter header

Here you can specify the email's subject line, the title, and how to display the
date.  You can also upload an image in place of the title for HTML newsletters.

The next step is to tell SunMailer where to pull content from.  Go to:

Administer > Site configuration > SunMailer > Configure SunMailer > Newsletter sections

Here you can add, edit, or delete newsletter sections.  Each section has a name,
a view, and a weight for ordering the sections.  SunMailer will use the name as
the heading for the section and the view for to pull content for the section.

B.  Sending

If you are sending SunMailer for the first time, go to:

Administer > Site configuration > SunMailer > Send email > Newsletter settings

SunMailer's newsletter will only include content that appears after the cutoff
date.  This date is initially set to the time when SunMailer was installed.

You may only need to set the cutoff date once; every time a newsletter is sent,
SunMailer automatically updates the cutoff date to the time when the newsletter
was sent.  This ensures the next newsletter will contain only new content.

You can also add a customized message to the header of the newsletter here.

You can schedule the newsletter to be sent via cron or manually send it.  Go to:

Administer > Site configuration > SunMaller > Send email > Schedule newsletter

Administer > Site configuration > SunMailer > Send email > Preview and send newsletter
