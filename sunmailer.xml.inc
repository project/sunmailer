<?php

/**
 * @file
 * Builds the XML newsletter and the XSL to transform it.
 */

/**
 * Generates the XML for a newsletter with selected sections.
 */
function sunmailer_xml_newsletter($sids, $reset = FALSE) {
  static $cached = FALSE;
  static $no_section_title = FALSE;
  static $sections = array();
  // Build all sections once, then cache them.
  if (!$cached || $reset) {
    $cached = TRUE;
    // Impersonate an anonymous user.
    global $user;
    $original_user = $user;
    drupal_save_session(FALSE);
    $user = user_load(array('uid' => 0));
    // Build the sections.
    $sections = sunmailer_get_sections();
    foreach ($sections as &$sectionn) { // not a typo, see PHP's foreach() documentation
      sunmailer_get_section_nodes($sectionn);
    }
    if (count($sections) == 1) {
      $no_section_title = TRUE;
    }
    // Revert to original user.
    $user = $original_user;
    drupal_save_session(TRUE);
  }
  // Get XML for each section.
  $xml = '';
  foreach ($sections as $sid => $section) {
    if (in_array($sid, $sids)) {
      $xml .= sunmailer_xml_section($section, $no_section_title);
    }
  }
  // Return nothing if no sections have content.
  if (!$xml) {
    return NULL;
  }
  // Build newsletter XML.
  $xml = sunmailer_xml_header() . $xml . sunmailer_xml_footer();
  $doc = new DOMDocument();
  $doc->loadXML($xml);
  return $doc;
}

/**
 * Retrieves the nodes for an individual section.
 */
function sunmailer_get_section_nodes(&$section) {
  $view = views_get_view($section['view']);
  $view->execute_display($section['display'], $section['args'] ? explode('/', $section['args']) : array());
  $section['nodes'] = array();
  foreach ($view->result as $row) {
    if (!$row->nid) {
      continue;
    }
    $node = node_load($row->nid);
    if ($node->created < variable_get('sunmailer_cutoff', 0)) {
      continue;
    }
    $section['nodes'][] = $node;
  }
  return $section;
}

/**
 * Generates the XML for the header.
 */
function sunmailer_xml_header() {
  static $cached = FALSE;
  static $xml;
  if ($cached) {
    return $xml;
  }
  $cached = TRUE;
  // Retrieve the header information.
  $encoding = variable_get('sunmailer_char_set', 'utf-8');
  $title = variable_get('sunmailer_title', variable_get('site_name', t('SunMailer')));
  global $base_url;
  if ($logo_fid = variable_get('sunmailer_logo_fid', 0)) {
    $path = db_query("SELECT uri FROM {file} WHERE fid = :fid", array(':fid' => $logo_fid))->fetchField();
    $logo = "$base_url/$path";
  }
  $date_format = variable_get('sunmailer_date_format', 'l, F j, Y');
  if ($date_format) {
    $date = date($date_format);
  }
  $message = variable_get('sunmailer_message', '');
  // Build the header XML.
  $xml  = '<?xml version="1.0" encoding="' . $encoding .'"?>';
  $xml .= '<newsletter>';
  $xml .= "<title><![CDATA[$title]]></title>";
  if ($logo) {
    $xml .= "<logo><![CDATA[$logo]]></logo>";
  }
  $xml .= "<link><![CDATA[$base_url]]></link>";
  if ($date) {
    $xml .= "<date><![CDATA[$date]]></date>";
  }
  if ($message) {
    $xml .= "<message><![CDATA[$message]]></message>";
  }
  return $xml;
}

/**
 * Generates the XML for a section.
 */
function sunmailer_xml_section($section, $no_title) {
  // Return nothing if the section has no content.
  if (!count($section['nodes'])) {
    return '';
  }
  // Build the section XML.
  $xml = '<section>';
  if (!$no_title) {
    $xml .= "<name><![CDATA[{$section['name']}]]></name>";
  }
  foreach ($section['nodes'] as $node) {
    $xml .= sunmailer_xml_content($node);
  }
  $xml .= '</section>';
  return $xml;
}

/**
 * Generates the XML for a content piece.
 */
function sunmailer_xml_content($node) {
  // Retrieve the content information.
  $title = check_plain($node->title);
  global $base_url;
  $link = $base_url . url("node/{$node->nid}");
  if ($node->uid) {
    $author = check_plain($node->name);
    $authorlink = $base_url . url("user/{$node->uid}");
  }
  $date = format_date($node->created);
  // Build the content XML.
  $xml = '<content>';
  $xml .= "<title><![CDATA[$title]]></title>";
  $xml .= "<link><![CDATA[$link]]></link>";
  if ($author) {
    $xml .= "<author><![CDATA[$author]]></author>";
    $xml .= "<authorlink><![CDATA[$authorlink]]></authorlink>";
  }
  $xml .= "<date><![CDATA[$date]]></date>";
  $xml .= "<teaser><![CDATA[{$node->teaser}]]></teaser>";
  $xml .= '</content>';
  return $xml;
}

/**
 * Generates the XML for the footer.
 */
function sunmailer_xml_footer() {
  static $cached = FALSE;
  static $xml;
  if ($cached) {
    return $xml;
  }
  $cached = TRUE;
  // Retrieve the footer information.
  global $base_url;
  $unsubscribe_url = $base_url . url('sunmailer/unsubscribe');
  $unsubscribe_link = l($unsubscribe_url, $unsubscribe_url);
  $password_url = $base_url . url('user/password');
  $password_link = l($password_url, $password_url);
  $notice = "To unsubscribe, go to $unsubscribe_link. Forgot your password? Reset it at $password_link.";
  // Build the footer XML.
  $xml = "<notice><![CDATA[$notice]]></notice>";
  $xml .= '</newsletter>';
  return $xml;
}

/**
 * Transforms XML into text or HTML.
 */
function sunmailer_transform_xml($xml, $format) {
  // Load XSL.
  $path = db_query("SELECT uri FROM {file} WHERE fid = :fid", array(':fid' => $format['fid']))->fetchField();
  $doc = new DOMDocument();
  $doc->load($path);
  $xslt = new XSLTProcessor();
  $xslt->importStyleSheet($doc);
  // Transform XML, do some extra processing for text formats.
  $newsletter = $xslt->transformToXML($xml);
  if (!$format['html']) {
    $newsletter = strip_tags($newsletter);
    $newsletter = html_entity_decode($newsletter);
  }
  return $newsletter;
}