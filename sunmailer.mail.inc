<?php

/**
 * @file
 * Schedule the newsletter or preview and send it manually.  Provides statistics.
 */

/**
 * Checks if SunMailer can be sent and finds the next scheduled date.
 */
function sunmailer_check_schedule() {
  // Check if SunMailer can be sent.
  $now = time();
  $next = variable_get('sunmailer_schedule_next', $now + 1);
  $once = variable_get('sunmailer_schedule_single', $now + 1);
  if (($now < $next) && ($now < $once)) {
    return FALSE;
  }
  // Find the next scheduled date.
  if ($now >= $next) {
    $date = new DateTime(date('Y-m-d H:i', $next));
    switch (variable_get('sunmailer_schedule_type', 'none')) {
    case 'day':
      $num = variable_get('sunmailer_day_interval', 1);
      $date->modify("+$num days");
      break;
    case 'week':
      $days = variable_get('sunmailer_week_days', array(7));
      while (TRUE) {
        $date->modify('+1 day');
        $day = $date->format('N');
        if ($day == 0) {
          $num = variable_get('sunmailer_week_interval', 1) - 1;
          $date->modify("+$num weeks");
        }
        if (in_array($day, $days)) {
          break;
        }
      }
      break;
    case 'month':
      $days = variable_get('sunmailer_month_days', array(1));
      while (TRUE) {
        $date->modify('+1 day');
        $day = $date->format('j');
        if (in_array($day, $days)) {
          break;
        }
      }
      break;
    }
    variable_set('sunmailer_schedule_next', $date->format('U'));
  }
  variable_del('sunmailer_schedule_single');
  return TRUE;
}

/**
 * Checks the newsletter for errors.
 */
function sunmailer_check_newsletter() {
  module_load_install('sunmailer');
  module_load_include('inc', 'sunmailer', 'sunmailer.xml');
  $errors = array();
  // Check the requirements.
  $requirements = module_invoke('sunmailer', 'requirements', 'runtime');
  if (count($requirements)) {
    foreach ($requirements as $requirement) {
      $errors[] = $requirement['description'];
    }
    return $errors;
  }
  // Check the views.
  $sections = sunmailer_get_sections();
  foreach ($sections as $section) {
    $name = $section['view'];
    $display = $section['display'];
    $view = views_get_view($name);
    if (!$view) {
      $errors[] = t('View !name does not exist.', array('!name' => $name));
      continue;
    }
    if ($view->base_table != 'node') {
      $errors[] = t('View !name is not a node view.', array('!name' => $name));
      continue;
    }
    if ($view->disabled) {
      $errors[] = t('View !name is disabled.', array('!name' => $name));
      continue;
    }
    if (!array_key_exists($display, $view->display)) {
      $errors[] = t('View !name does not contain the display !display.', array('!name' => $name, '!display' => $display));
      continue;
    }
  }
  if (count($errors)) {
    return $errors;
  }
  // Check the XSL files.
  $xml = sunmailer_xml_newsletter(array_keys(sunmailer_get_sections()));
  if (!$xml) {
    return array();
  }
  $formats = sunmailer_get_formats();
  foreach ($formats as $format) {
    $row = db_query("SELECT filename, uri FROM {file} WHERE fid = :fid", array(':fid' => $format['fid']))->fetchObject();
    $xsl_doc = new DOMDocument();
    if (!$xsl_doc->load($row->uri)) {
      $errors[] = t('!name is not a valid XSL file.', array('!name' => $row->filename));
      continue;
    }
    $xslt = new XSLTProcessor();
    $xslt->importStyleSheet($xsl_doc);
    if (!$xslt->transformToXml($xml)) {
      $errors[] = t('An XSLT error occurred when transforming the XML with !name.', array('!name' => $row->filename));
    }
  }
  return $errors;
}

/**
 * Mails the newsletter.
 */
function sunmailer_send_newsletter() {
  module_load_include('inc', 'sunmailer', 'sunmailer.xml');
  // Check the newsletter.
  $errors = sunmailer_check_newsletter();
  if (count($errors)) {
    watchdog('mail', 'Error(s) sending the newsletter: ' . implode(' ', $errors), array(), WATCHDOG_ERROR);
    return;
  }
  $xml = sunmailer_xml_newsletter(array_keys(sunmailer_get_sections()));
  if (!$xml) {
    watchdog('mail', 'Skipping the newsletter; it has no new content.');
    return;
  }
  // Perform time-based logic.
  watchdog('mail', 'Sending the email newsletter');
  variable_set('sunmailer_last_sent', time());
  variable_set('sunmailer_cutoff', time());
  // Set up the mailer.
  $mailer = sunmailer_get_mailer();
  $subject = variable_get('sunmailer_subject', variable_get('site_name', t('SunMailer')));
  $formats = sunmailer_get_formats();
  $alt_format = $formats[SUNMAILER_TEXT_FORMAT];
  // Get the newsletter XML for each combination of sections.
  $subscriptions = sunmailer_get_subscriptions();
  foreach ($subscriptions as $sid_list => $groups) {
    $sids = explode(',', $sid_list);
    $xml = sunmailer_xml_newsletter($sids);
    if (!$xml) {
      continue;
    }
    // Construct the newsletter in each format and send it.
    foreach ($groups as $xid => $addresses) {
      $format = $formats[$xid];
      $body = sunmailer_transform_xml($xml, $format);
      $is_html = $format['html'];
      if ($is_html) {
        $alt_notice = 'Note: You are viewing this as text because your email client does not support HTML.';
        $alt_body = "$alt_notice\n\n" . sunmailer_transform_xml($xml, $alt_format);
      }
      sunmailer_send_mail($mailer, $addresses, $subject, $body, $is_html, $alt_body);
    }
  }
  watchdog('mail', 'Sent the email newsletter');
}

/**
 * Implements hook_cron().
 */
function sunmailer_cron() {
  module_load_include('inc', 'sunmailer', 'sunmailer.mail');
  if (!sunmailer_check_schedule()) {
    return;
  }
  sunmailer_send_newsletter();
}


/**
 * Sets the element type based on which Date modules are enabled.
 */
function _sunmailer_set_date_type(&$element) {
  if (module_exists('date_api')) {
    $element['#type'] = module_exists('date_popup') ? 'date_popup' : 'date_select';
    $element['#format'] = 'Y-m-d H:i';
  }
  else {
    $element['#type'] = 'textfield';
    $element['#element_validate'] = array('_sunmailer_date_validate');
  }
}

/**
 * Validation callback for default SunMailer date field.
 */
function _sunmailer_date_validate($element, &$form_state) {
  if (!strtotime($element['#value'])) {
    form_error($element, t('Invalid date format.'));
  }
}

/**
 * Generates a form for newsletter settings.
 */
function sunmailer_mail_settings() {
  $cutoff = variable_get('sunmailer_cutoff', 0);
  $form = array();
  $form['sunmailer_cutoff'] = array(
    '#title' => t('Cutoff date'),
    '#description' => t('The newsletter will exclude content published before this date'),
    '#required' => TRUE,
    '#default_value' => format_date($cutoff, 'custom', 'Y-m-d H:i'),
  );
  _sunmailer_set_date_type($form['sunmailer_cutoff']);
  $form['sunmailer_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#description' => t('This message will appear in the header, with HTML tags stripped for text newsletters'),
    '#default_value' => variable_get('sunmailer_message', ''),
  );
  $form['sunmailer_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Submission callback for newsletter settings form.
 */
function sunmailer_mail_settings_submit($form, &$form_state) {
  $values = $form_state['values'];
  variable_set('sunmailer_message', filter_xss(trim($values['sunmailer_message'])));
  variable_set('sunmailer_cutoff', strtotime($values['sunmailer_cutoff']));
}

/**
 * Generates a form to schedule the newsletter.
 */
function sunmailer_mail_schedule() {
  $form = array();
  $tomorrow = strtotime(date('Y-m-d', time() + 24 * 60 * 60));
  // Send the newsletter on a recurring basis.
  $schedule_type = variable_get('sunmailer_schedule_type', 'none');
  $schedule_next = variable_get('sunmailer_schedule_next', $tomorrow);
  $form['recurring'] = array(
    '#type' => 'fieldset',
    '#title' => t('Recurring Schedule'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['recurring']['schedule_type'] = array(
    '#type' => 'radios',
    '#title' => t('Frequency'),
    '#description' => t('Use weekly to send the newsletter on certain weekdays (e.g.: daily except weekends)'),
    '#options' => array(
      'none' => t('None'),
      'day' => t('Daily'),
      'week' => t('Weekly'),
      'month' => t('Monthly'),
    ),
    '#default_value' => $schedule_type,
  );
  $form['recurring']['schedule_next'] = array(
    '#title' => t('Next date'),
    '#description' => t('The newsletter will be sent next on or after this date - the exact date depends on when cron runs'),
    '#required' => TRUE,
    '#default_value' => format_date($schedule_next, 'custom', 'Y-m-d H:i'),
  );
  _sunmailer_set_date_type($form['recurring']['schedule_next']);
  $form['recurring']['day'] = array(
    '#type' => 'fieldset',
    '#title' => 'Daily settings',
    '#collapsible' => TRUE,
    '#collapsed' => $schedule_type != 'day',
  );
  $form['recurring']['day']['day_interval'] = array(
    '#type' => 'select',
    '#title' => t('Interval'),
    '#description' => t('One is daily, two is bi-daily, etc...'),
    '#options' => drupal_map_assoc(array(1, 2, 3, 4, 5, 6)),
    '#default_value' => variable_get('sunmailer_day_interval', 1),
  );
  $form['recurring']['week'] = array(
    '#type' => 'fieldset',
    '#title' => 'Weekly settings',
    '#collapsible' => TRUE,
    '#collapsed' => $schedule_type != 'week',
  );
  $form['recurring']['week']['week_interval'] = array(
    '#type' => 'select',
    '#title' => t('Interval'),
    '#description' => t('One is weekly, two is bi-weekly, etc...'),
    '#options' => drupal_map_assoc(array(1, 2, 3, 4)),
    '#default_value' => variable_get('sunmailer_week_interval', 1),
  );
  $form['recurring']['week']['week_days'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Days of week',
    '#options' => array(
      7 => t('Sunday'),
      1 => t('Monday'),
      2 => t('Tuesday'),
      3 => t('Wednesday'),
      4 => t('Thursday'),
      5 => t('Friday'),
      6 => t('Saturday'),
    ),
    '#default_value' => variable_get('sunmailer_week_days', array(7)),
  );
  $form['recurring']['monthly'] = array(
    '#type' => 'fieldset',
    '#title' => t('Monthly settings'),
    '#collapsible' => TRUE,
    '#collapsed' => $schedule_type != 'month',
  );
  $form['recurring']['monthly']['month_days'] = array(
    '#type' => 'textfield',
    '#title' => t('Days of month'),
    '#description' => t('Enter a comma-delimited list of days (e.g.: 1,16)'),
    '#default_value' => implode(',', variable_get('sunmailer_month_days', array(1))),
  );
  // Send the newsletter once.
  $schedule_single = variable_get('sunmailer_schedule_single', $tomorrow);
  $form['single'] = array(
    '#type' => 'fieldset',
    '#title' => t('Send once'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['single']['send_single'] = array(
    '#type' => 'checkbox',
    '#title' => t('Schedule the newsletter to be sent once'),
    '#default_value' => variable_get('sunmailer_schedule_single', 0),
  );
  $form['single']['schedule_single'] = array(
    '#title' => t('Date'),
    '#description' => t('The newsletter will be sent the first time cron runs after this date'),
    '#required' => TRUE,
    '#default_value' => format_date($schedule_single, 'custom', 'Y-m-d H:i'),
  );
  _sunmailer_set_date_type($form['single']['schedule_single']);
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Validation callback for newsletter scheduling form.
 */
function sunmailer_mail_schedule_validate($form, &$form_state) {
  $values = $form_state['values'];
  $day_exists = FALSE;
  foreach ($values['week_days'] as $day) {
    if ($day) {
      $day_exists = TRUE;
      break;
    }
  }
  if (!$day_exists) {
    form_set_error('week_days', t('Select at least one day.'));
  }
  $days_raw = explode(',', $values['month_days']);
  $days = array();
  foreach ($days_raw as $day) {
    $day = trim($day);
    if ($day === '') {
      continue;
    }
    if (!is_numeric($day) || ($day < 1) || ($day > 31)) {
      form_set_error('month_days', t('@day is not a valid day of the month.', array('@day' => $day)));
    }
    $days[] = $day;
  }
  if (!count($days)) {
    form_set_error('month_days', t('Enter at least one day.'));
  }
}

/**
 * Moves the date forward until it aligns with the schedule.
 */
function _sunmailer_initialize_date($date_string) {
  $date = new DateTime($date_string);
  switch (variable_get('sunmailer_schedule_type', 'none')) {
  case 'week':
    $days = variable_get('sunmailer_week_days', array(7));
    while (TRUE) {
      $day = $date->format('w'); // Use PHP 5.1's 'N' in Drupal 7.
      if (in_array($day, $days) || (($day == 0) && in_array(7, $days))) {
        break;
      }
      $date->modify('+1 day');
    }
    break;
  case 'month':
    $days = variable_get('sunmailer_month_days', array(1));
    while (TRUE) {
      $day = $date->format('j');
      if (in_array($day, $days)) {
        break;
      }
      $date->modify('+1 day');
    }
    break;
  }
  return $date->format('U');
}

/**
 * Submission callback for newsletter scheduling form.
 */
function sunmailer_mail_schedule_submit($form, &$form_state) {
  $values = $form_state['values'];
  // Process recurring schedule.
  variable_set('sunmailer_schedule_type', $values['schedule_type']);
  variable_set('sunmailer_day_interval', $values['day_interval']);
  variable_set('sunmailer_week_interval', $values['week_interval']);
  $days = array();
  foreach ($values['week_days'] as $day) {
    if ($day) {
      $days[] = $day;
    }
  }
  variable_set('sunmailer_week_days', $days);
  $days_raw = explode(',', $values['month_days']);
  $days = array();
  foreach ($days_raw as $day_raw) {
    $day = (int)trim($day_raw);
    $days[$day] = $day;
  }
  sort($days);
  variable_set('sunmailer_month_days', $days);
  if ($values['schedule_type'] == 'none') {
    variable_del('sunmailer_schedule_next');
  }
  else {
    $next = _sunmailer_initialize_date($values['schedule_next']);
    variable_set('sunmailer_schedule_next', $next);
  }
  // Process single schedule.
  if ($values['send_single']) {
    variable_set('sunmailer_schedule_single', strtotime($values['schedule_single']));
  }
  else {
    variable_del('sunmailer_schedule_single');
  }
}

/**
 * Displays a preview of the newsletter.
 */
function sunmailer_newsletter_preview($xid) {
  module_load_include('inc', 'sunmailer', 'sunmailer.xml');
  $xml = sunmailer_xml_newsletter(array_keys(sunmailer_get_sections()));
  if (!$xml) {
    print 'The email newsletter currently has no content';
    exit;
  }
  if (!$xid) {
    header('Content-type: text/xml');
    print $xml->saveXML();
    exit;
  }
  $formats = sunmailer_get_formats();
  $format = $formats[$xid];
  $preview = sunmailer_transform_xml($xml, $format);
  if (!$format['html']) {
    $preview = nl2br(htmlentities($preview));
  }
  print $preview;
  exit;
}

/**
 * Checks the last time SunMailer was sent.
 */
function sunmailer_check_last_sent($notify) {
  $last_sent = variable_get('sunmailer_last_sent', 0);
  if (!$last_sent) {
    return TRUE;
  }
  $last_delta = time() - $last_sent;
  $delay = 60 * variable_get('sunmailer_delay', 0);
  if ($last_delta > $delay) {
    if ($notify) {
      drupal_set_message(t('An email was last sent @date.', array('@date' => date('l, F j, Y g:i:s A', $last_sent))));
    }
    return TRUE;
  }
  if ($notify) {
    $message = t('An email was sent @delta seconds ago.  Please wait @delay seconds before sending another.', array('@delta' => $last_delta, '@delay' => $delay));
    drupal_set_message($message, 'warning');
  }
  return FALSE;
}

/**
 * Generates a form to send the newsletter.
 */
function sunmailer_mail_newsletter() {
  module_load_include('inc', 'sunmailer', 'sunmailer.xml');
  // Check for any newsletter errors.
  $errors = sunmailer_check_newsletter();
  if (count($errors)) {
    drupal_set_title('Error');
    $html = '';
    foreach ($errors as $error) {
      $html .= "<p>$error</p>";
    }
    return array('error' => array('#value' => $html));
  }
  // Present a preview of the newsletter in every format.
  $xml = sunmailer_xml_newsletter(array_keys(sunmailer_get_sections()));
  if (!$xml) {
    drupal_set_message(t('The email newsletter currently has no content.'));
    return;
  }
  $formats = sunmailer_get_formats();
  $format = $formats[SUNMAILER_TEXT_FORMAT];
  $preview .= "<p><strong>{$format['name']} preview</strong></p>";
  $preview .= nl2br(htmlentities(sunmailer_transform_xml($xml, $format)));
  $preview .= '<hr /><ul>';
  foreach ($formats as $xid => $format) {
    if ($xid != SUNMAILER_TEXT_FORMAT) {
      $preview .= '<li>' . l("{$format['name']} preview", "sunmailer/preview/$xid") . '</li>';
    }
  }
  if (user_access('administer sunmailer')) {
    $preview .= '<li>' . l('XML preview', 'sunmailer/preview/0') . '</li>';
  }
  $preview .= '</ul><hr />';
  // Check when an email was last sent.
  $can_send = sunmailer_check_last_sent(TRUE);
  // Build the form.
  $form = array();
  $form['text'] = array(
    '#prefix' => '<div>',
    '#value' => $preview,
    '#suffix' => '</div>',
  );
  $message = variable_get('sunmailer_message', '');
  if ($message) {
    $form['delete_message'] = array(
      '#type' => 'checkbox',
      '#title' => t('Delete the message in the header for future newsletters'),
    );
  }
  if (!$can_send) {
    $form['override'] = array(
      '#type' => 'checkbox',
      '#title' => t('Override the time limit to send another email'),
    );
  }
  $form['confirm'] = array(
    '#type' => 'checkbox',
    '#title' => t('Confirm that you want to send the newsletter'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );
  return $form;
}

/**
 * Validation callback for form to send the newsletter.
 */
function sunmailer_mail_newsletter_validate($form, &$form_state) {
  $values = $form_state['values'];
  if (!$values['confirm']) {
    form_set_error('confirm', t('Cannot send without confirmation.'));
  }
  if (!sunmailer_check_last_sent(FALSE) && !$values['override']) {
    form_set_error('override', t('An email has been sent recently.'));
  }
}

/**
 * Submission callback for form to send the newsletter.
 */
function sunmailer_mail_newsletter_submit($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['override']) {
    watchdog('mail', 'Overriding SunMailer\'s time limit for sending another email');
  }
  sunmailer_mail_newsletter();
  if ($values['delete_message']) {
    variable_del('sunmailer_message');
  }
  $form_state['redirect'] = user_access('access site reports') ? 'admin/reports/dblog' : 'admin/settings/sunmailer';
}

/**
 * Creates a form to send a custom text email message.
 */
function sunmailer_mail_custom(&$form_state = NULL) {
  // Check when an email was last sent.
  $can_send = sunmailer_check_last_sent(TRUE);
  // Build the form.
  $form = array();
  if (isset($form_state['storage']['preview'])) {
    $form['#prefix'] = $form_state['storage']['preview'];
  }
  $form['message'] = array(
    '#type' => 'fieldset',
    '#title' => t('Custom message'),
    '#description' => t('Message to email to all subscribers'),
  );
  $form['message']['subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => isset($form_state) ? $form_state['values']['subject'] : '',
  );
  $form['message']['body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#description' => t('HTML tags will be stripped from the text version'),
    '#default_value' => isset($form_state) ? $form_state['values']['body'] : '',
  );
  if (!$can_send) {
    $form['override'] = array(
      '#type' => 'checkbox',
      '#title' => t('Override the time limit to send another email'),
    );
  }
  $form['confirm'] = array(
    '#type' => 'checkbox',
    '#title' => t('Confirm that you want to send this email'),
    '#default_value' => FALSE,
  );
  $form['preview'] = array(
    '#type' => 'submit',
    '#value' => t('Preview'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );
  return $form;
}

/**
 * Validation callback for the form to send a custom email.
 */
function sunmailer_mail_custom_validate($form, &$form_state) {
  $values = $form_state['values'];
  if (!$values['subject']) {
    form_set_error('subject', t('Subject is blank.'));
  }
  if (!$values['body']) {
    form_set_error('body', t('Body is blank.'));
  }
  if ($form_state['clicked_button']['#value'] != t('Preview')) {
    if (!$values['confirm']) {
      form_set_error('confirm', t('Cannot send without confirmation.'));
    }
    if (!sunmailer_check_last_sent(FALSE) && !$values['override']) {
      form_set_error('override', t('An email has been sent recently.'));
    }
  }
}

/**
 * Submission callback for the form to send a custom email.
 */
function sunmailer_mail_custom_submit($form, &$form_state) {
  $values = $form_state['values'];
  $subject = check_plain($values['subject']);
  $body = filter_xss($values['body']);
  $alt_body = strip_tags($body);
  // Send the email in text.
  if ($form_state['clicked_button']['#value'] == t('Send')) {
    // Perform time-based logic.
    variable_set('sunmailer_last_sent', time());
    if ($values['override']) {
      watchdog('mail', 'Overriding SunMailer\'s time limit for sending another email');
    }
    // Send the email.
    watchdog('mail', 'Sending a custom email message');
    $mailer = sunmailer_get_mailer();
    $addresses = sunmailer_get_all_subscribers();
    sunmailer_send_mail($mailer, $addresses, $subject, $body, TRUE, $alt_body);
    watchdog('mail', 'Sent a custom email message');
    // Redirect.
    unset($form_state['storage']);
    $form_state['redirect'] = user_access('access site reports') ? 'admin/reports/dblog' : 'admin/settings/sunmailer';
  }
  // Store a preview of the email newsletter.
  else {
    $form_state['storage']['preview'] = "<h3>$subject</h3><ul><li><strong>HTML:</strong> $body</li><li><strong>Text:</strong> $alt_body</li></ul>";
  }
}

/**
 * Calculates subscription statistics.
 */
function get_sunmailer_stats() {
  // Initialize the statistics.
  $sections = sunmailer_get_sections();
  $formats = sunmailer_get_formats();
  $stats['total'] = 0;
  $stats['splits']['section'] = array();
  foreach ($sections as $section) {
    $stats['splits']['section'][$section['name']] = 0;
  }
  $stats['splits']['format'] = array();
  foreach ($formats as $format) {
    $stats['splits']['format'][$format['name']] = 0;
  }
  // Calculate the statistics.
  $subscriptions = sunmailer_get_subscriptions();
  foreach ($subscriptions as $sid_list => $sections_group) {
    $sids = explode(',', $sid_list);
    foreach ($sections_group as $xid => $format_group) {
      $count = count($format_group);
      $stats['total'] += $count;
      $stats['splits']['format'][$formats[$xid]['name']] += $count;
      foreach ($sids as $sid) {
        $stats['splits']['section'][$sections[$sid]['name']] += $count;
      }
    }
  }
  return $stats;
}

/**
 * Displays a page of statistics.
 */
function sunmailer_stats_page() {
  return theme('sunmailer_stats', get_sunmailer_stats());
}

/**
 * Default theme for statistics.
 */
function theme_sunmailer_stats($stats) {
  $html = '<h3>' . t('Total subscribers') . '</h3>';
  $html .= '<ul><li>' . check_plain($stats['total']) . '</li></ul>';
  foreach ($stats['splits'] as $name => $split) {
    $html .= '<h4>' . t('By @name', array('@name' => $name)) . '</h4>';
    $html .= '<ul>';
    foreach ($split as $name => $count) {
      $html .= '<li>' . t('@name: @count', array('@name' => $name, '@count' => $count)) . '</li>';
    }
    $html .= '</ul>';
  }
  return $html;
}
