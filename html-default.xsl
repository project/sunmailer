<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>
  <xsl:template match="/newsletter">
    <html>
      <body>
        <table width="600" style="border: 1px lightgray solid; padding: 5px; background-color: white">
          <tr>
            <td style="border-bottom: 1px solid black; text-align: center; padding 5px 0px">
              <xsl:choose> 
                <xsl:when test="logo">
                  <a>
                    <xsl:attribute name="href">
                      <xsl:value-of select="link" disable-output-escaping="yes"/>
                    </xsl:attribute>
                    <img style="border-width: 0px">
                      <xsl:attribute name="src">
                        <xsl:value-of select="logo" disable-output-escaping="yes"/>
                      </xsl:attribute>
                      <xsl:attribute name ="alt">
                        <xsl:value-of select="title" disable-output-escaping="yes"/>
                      </xsl:attribute>
                    </img>
                  </a>
                </xsl:when>
                <xsl:otherwise>
                  <h2>
                    <a>
                      <xsl:attribute name="href">
                        <xsl:value-of select="link" disable-output-escaping="yes"/>
                      </xsl:attribute>
                      <xsl:value-of select="title" disable-output-escaping="yes"/>
                    </a>                      
                  </h2>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:if test="date">
                <p style="font-size: small"><strong><xsl:value-of select="date" disable-output-escaping="yes"/></strong></p>
              </xsl:if>
            </td>
          </tr>
          <xsl:if test="message">
            <tr>
              <td style="border-bottom: 1px solid black; text-align: center; padding: 5px 0px">
                <p><xsl:value-of select="message" disable-output-escaping="yes"/></p>
              </td>
            </tr>
          </xsl:if>
          <xsl:for-each select="section">
            <xsl:if test="name">
              <tr style="border: 0px; padding: 0px; margin: 0px">
                <th style="border-top: 1px solid black; border-bottom: 5px solid black; padding: 5px 0px; font-size: large">
                  <p><xsl:value-of select="name" disable-output-escaping="yes"/></p>
                </th> 
              </tr>
            </xsl:if>
            <xsl:for-each select="content"> 
              <tr style="border: 0px; padding: 0px; margin: 0px">
                <td style="border-bottom: 1px solid black; padding: 5px 0px">
                  <p>
                    <a>
                      <xsl:attribute name="href">
                        <xsl:value-of select="link" disable-output-escaping="yes"/>
                      </xsl:attribute>
                      <xsl:value-of select="title" disable-output-escaping="yes"/>
                    </a><br />
                    <xsl:if test="author">
                      <a>
                        <xsl:attribute name="href">
                          <xsl:value-of select="authorlink" disable-output-escaping="yes"/>
                        </xsl:attribute>
                        <xsl:value-of select="author" disable-output-escaping="yes"/>
                      </a><br />
                    </xsl:if>
                    <i><xsl:value-of select="date" disable-output-escaping="yes"/></i><br/>
                    <xsl:value-of select="teaser" disable-output-escaping="yes"/>
                  </p>
                </td>
              </tr>
            </xsl:for-each>
          </xsl:for-each>
          <tr>
            <td style="border: 0px; padding: 5px 0px">
              <p><xsl:value-of select="notice" disable-output-escaping="yes"/></p>
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
