<?php

/**
 * @file
 * Configuration for PHPMailer, newsletter header, sections, formats, and subscriptions.
 */

/**
 * Generates a form for PHPMailer settings.
 */
function sunmailer_admin_settings() {
  $form = array();
  // Important settings.
  $form['phpmailer_path'] = array(
    '#type' => 'textfield',
    '#title' => t('PHPMailer path'),
    '#description' => t('Path to the directory containing class.phpmailer.php - leave blank if PHPMailer is in PHP\'s include path'),
    '#default_value' => variable_get('sunmailer_phpmailer_path', ''),
  );
  $form['test_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Test email'),
    '#description' => t('Email address to receive test emails'),
    '#default_value' => variable_get('sunmailer_test_email', ''),
  );
  $form['send'] = array(
    '#type' => 'submit',
    '#value' => t('Send test email'),
  );

  // Basic settings.
  $form['basic'] = array(
    '#type' => 'fieldset',
    '#title' => t('Basic settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['basic']['from_name'] = array(
    '#type' => 'textfield',
    '#title' => t('From name'),
    '#description' => t('Name of the sender'),
    '#required' => TRUE,
    '#default_value' => variable_get('sunmailer_from_name', ''),
  );
  $form['basic']['from_email'] = array(
    '#type' => 'textfield',
    '#title' => t('From email'),
    '#description' => t('Email of the sender'),
    '#required' => TRUE,
    '#default_value' => variable_get('sunmailer_from_email', ''),
  );
  $form['basic']['host'] = array(
    '#type' => 'textfield',
    '#title' => t('Host'),
    '#description' => t('URL or IP address for the server sending the message'),
    '#required' => TRUE,
    '#default_value' => variable_get('sunmailer_host', 'localhost'),
  );
  $form['basic']['port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port'),
    '#description' => t('Port the server sends the message on'),
    '#required' => TRUE,
    '#default_value' => variable_get('sunmailer_port', 25),
  );

  // Mail settings.
  $form['mail'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mail settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['mail']['method'] = array(
    '#type' => 'radios',
    '#title' => t('Mailing method'),
    '#description' => t('Selects the method used to send the newsletter'),
    '#required' => TRUE,
    '#options' => array(
      'mail' => 'Mail',
      'sendmail' => 'Sendmail',
      'smtp' => 'SMTP',
    ),
    '#default_value' => variable_get('sunmailer_method', 'mail'),
  );
  // Sendmail settings.
  $form['mail']['sendmail_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Sendmail path'),
    '#description' => t('Path to the sendmail program'),
    '#default_value' => variable_get('sunmailer_sendmail_path', '/usr/bin/sendmail'),
  );
  // SMTP settings.
  $form['mail']['smtp_auth'] = array(
    '#type' => 'checkbox',
    '#title' => t('SMTP authentication'),
    '#description' => t('Check this to enable SMTP authentication'),
    '#default_value' => variable_get('sunmailer_smtp_auth', FALSE),
  );
  $form['mail']['smtp_user'] = array(
    '#type' => 'textfield',
    '#title' => t('SMTP username'),
    '#description' => t('Username for SMTP authentication'),
    '#default_value' => variable_get('sunmailer_smtp_user', ''),
  );
  $form['mail']['smtp_pass'] = array(
    '#type' => 'textfield',
    '#title' => t('SMTP password'),
    '#description' => t('Password for SMTP authentication'),
    '#default_value' => variable_get('sunmailer_smtp_pass', ''),
  );
  $form['mail']['smtp_secure'] = array(
    '#type' => 'radios',
    '#title' => t('SMTP security'),
    '#description' => t('Sets the encryption used for sending emails'),
    '#options' => array(
      '' => t('None'),
      'ssl' => 'SSL',
      'tls' => 'TLS',
    ),
    '#default_value' => variable_get('sunmailer_smtp_secure', ''),
  );

  // Advanced settings.
  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['advanced']['delay'] = array(
    '#type' => 'textfield',
    '#title' => t('Next email delay'),
    '#description' => t('Number of minutes to wait before allowing another email'),
    '#required' => TRUE,
    '#default_value' => variable_get('sunmailer_delay', 5),
  );
  $form['advanced']['retries'] = array(
    '#type' => 'textfield',
    '#title' => t('Resend attempts'),
    '#description' => t('Number of times to retry if the message fails to be sent'),
    '#required' => TRUE,
    '#default_value' => variable_get('sunmailer_retries', 2),
  );
  $form['advanced']['max_recipients'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum number of recipients'),
    '#description' => t('Emails with more than the maximum will be split into multiple emails.  Use 0 for no limit.'),
    '#required' => TRUE,
    '#default_value' => variable_get('sunmailer_max_recipients', 0),
  );
  $form['advanced']['word_wrap'] = array(
    '#type' => 'textfield',
    '#title' => t('Word wrap'),
    '#description' => t('Maximum number of text characters on one line'),
    '#required' => TRUE,
    '#default_value' => variable_get('sunmailer_word_wrap', 80),
  );
  $form['advanced']['char_set'] = array(
    '#type' => 'textfield',
    '#title' => t('Character set'),
    '#description' => t('Character encoding to use for emails'),
    '#required' => TRUE,
    '#default_value' => 'utf-8',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Validation callback for PHPMailer settings form.
 */
function sunmailer_admin_settings_validate($form, &$form_state) {
  $values = $form_state['values'];
  // Validate top-level settings and email test button.
  $found_phpmailer = FALSE;
  if ($path = trim($values['phpmailer_path'])) {
    if (file_exists("$path/class.phpmailer.php")) {
      $found_phpmailer = TRUE;
    }
  }
  else {
    $paths = explode(':', get_include_path());
    foreach ($paths as $path) {
      if (file_exists("$path/class.phpmailer.php")) {
        $found_phpmailer = TRUE;
        break;
      }
    }
  }
  if (!$found_phpmailer) {
    form_set_error('phpmailer_path', t('Cannot find PHPMailer.'));
  }
  if ($form_state['clicked_button']['#value'] == t('Send test email')) {
    if (!$values['test_email']) {
      form_set_error('test_email', t('Test email is required to send a test email.'));
    }
  }
  // Validate basic settings.
  if (!is_numeric($values['port']) || $values['port'] < 0) {
    form_set_error('port', t('Port must be a positive integer.'));
  }
  // Validate mail settings.
  switch ($values['method']) {
  case 'sendmail':
    if (!$values['sendmail_path']) {
      form_set_error('sendmail_path', t('Sendmail path is required if using Sendmail.'));
    }
    break;
  case 'smtp':
    if ($values['smtp_auth']) {
      if (!$values['smtp_user']) {
        form_set_error('smtp_user', t('SMTP username is required for SMTP authentication.'));
      }
      if (!$values['smtp_pass']) {
        form_set_error('smtp_pass', t('SMTP password is required for SMTP authentication.'));
      }
    }
    break;
  }
  // Validate advanced settings.
  if (!is_numeric($values['delay']) || $values['delay'] < 0) {
    form_set_error('delay', t('Next email delay must be a positive integer.'));
  }
  if (!is_numeric($values['retries']) || $values['retries'] < 0) {
    form_set_error('retries', t('Resend attempts must be a positive integer.'));
  }
  if (!is_numeric($values['max_recipients']) || $values['max_recipients'] < 0) {
    form_set_error('max_recipients', t('Maximum number of recipients must be a positive integer.'));
  }
  if (!is_numeric($values['word_wrap']) || $values['word_wrap'] < 0) {
    form_set_error('word_wrap', t('Word wrap must be a positive integer.'));
  }
}

/**
 * Submission callback for PHPMailer settings form.
 */
function sunmailer_admin_settings_submit($form, &$form_state) {
  $values = $form_state['values'];
  // Set top-level settings.
  variable_set('sunmailer_phpmailer_path', trim($values['phpmailer_path']));
  variable_set('sunmailer_test_email', $values['test_email']);
  // Set basic settings.
  variable_set('sunmailer_from_name', $values['from_name']);
  variable_set('sunmailer_from_email', $values['from_email']);
  variable_set('sunmailer_host', $values['host']);
  variable_set('sunmailer_port', (int)$values['port']);
  // Set mail settings.
  variable_set('sunmailer_method', $values['method']);
  variable_set('sunmailer_sendmail_path', $values['sendmail_path']);
  variable_set('sunmailer_smtp_auth', (bool)$values['smtp_auth']);
  variable_set('sunmailer_smtp_user', $values['smtp_user']);
  variable_set('sunmailer_smtp_pass', $values['smtp_pass']);
  variable_set('sunmailer_smtp_secure', $values['smtp_secure']);
  // Set advanced settings.
  variable_set('sunmailer_delay', (int)$values['delay']);
  variable_set('sunmailer_retries', (int)$values['retries']);
  variable_set('sunmailer_max_recipients', (int)$values['max_recipients']);
  variable_set('sunmailer_word_wrap', (int)$values['word_wrap']);
  variable_set('sunmailer_char_set', $values['char_set']);
  // Send a test email if requested.
  if ($form_state['clicked_button']['#value'] == t('Send test email')) {
    $mailer = sunmailer_get_mailer();
    $mailer->AddAddress(variable_get('sunmailer_test_email', ''));
    $mailer->Subject = 'SunMailer test';
    $mailer->Body = print_r($mailer, TRUE);
    $mailer->Send();
  }
}

/**
 * Generates a form for the newsletter header.
 */
function sunmailer_admin_header() {
  $form = array();
  $form['#attributes'] = array('enctype' => 'multipart/form-data');
  $form['subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#description' => t('Subject line for the email - leave blank to use the site name'),
    '#default_value' => variable_get('sunmailer_subject', ''),
  );
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('Leave blank to use the site name'),
    '#default_value' => variable_get('sunmailer_title', ''),
  );
  $form['logo'] = array(
    '#type' => 'file',
    '#title' => t('Logo'),
    '#description' => t('Logo to use for HTML newsletters - the title will become its alternate text'),
  );
  if (variable_get('sunmailer_logo_fid', 0)) {
    $form['delete_logo'] = array(
      '#type' => 'checkbox',
      '#title' => t('Delete logo'),
    );
  }
  $form['date_format'] = array(
    '#type' => 'textfield',
    '#title' => t('Date format'),
    '#description' => t('PHP <a href="http://www.php.net/manual/en/function.date.php">date format</a> for the date in the header - leave blank to exclude the date'),
    '#default_value' => variable_get('sunmailer_date_format', 'l, F j, Y'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Validation callback for newsletter header form.
 */
function sunmailer_admin_header_validate($form, &$form_state) {
  $values = $form_state['values'];
  if (!$values['delete_logo']) {
    if ($file = file_save_upload('logo', array('file_validate_is_image' => array()), file_directory_path())) {
      $form_state['values']['logo_file'] = $file;
    }
  }
}

/**
 * Submission callback for newsletter header form.
 */
function sunmailer_admin_header_submit($form, &$form_state) {
  $values = $form_state['values'];
  // Process text fields.
  if (trim($values['subject'])) {
    variable_set('sunmailer_subject', check_plain(trim($values['subject'])));
  }
  else {
    variable_del('sunmailer_subject');
  }
  if (trim($values['title'])) {
    variable_set('sunmailer_title', check_plain(trim($values['title'])));
  }
  else {
    variable_del('sunmailer_title');
  }
  $date_format = check_plain(trim($values['date_format']));
  variable_set('sunmailer_date_format', $date_format);
  if ($date_format) {
    drupal_set_message('Date format: ' . date($date_format));
  }
  // Process logo.
  if ($values['delete_logo']) {
    $fid = variable_get('sunmailer_logo_fid', 0);
    $path = db_query("SELECT uri FROM {file} WHERE fid = :fid", array(':fid' => $fid))->fetchField();
    variable_del('sunmailer_logo_fid');
    db_delete('file')->condition('fid', $fid)->execute();
    file_delete($path);
  }
  elseif ($file = $values['logo_file']) {
    $old_fid = variable_get('sunmailer_logo_fid', 0);
    $path = db_query("SELECT uri FROM {file} WHERE fid = :fid", array(':fid' => $old_fid))->fetchField();
    file_set_status($file, FILE_STATUS_PERMANENT);
    variable_set('sunmailer_logo_fid', (int)$file->fid);
    db_delete('file')->condition('fid', $old_fid)->execute();
    file_delete($path);
  }
}

/**
 * Autocomplete callback for views.
 */
function sunmailer_autocomplete_views($view_string = '', $display_string = '') {
  $views = views_get_all_views();
  $matches = array();
  foreach ($views as $view) {
    if ($view->disabled) {
      continue;
    }
    if ($view->base_table != 'node') {
      continue;
    }
    foreach ($view->display as $did => $display) {
      $match = "{$view->name}/$did";
      $string = $view_string . ($display_string ? "/$display_string" : '');
      $match_display = "$match ({$display->display_title})";
      if (strpos($match, $string) === 0) {
        $matches[$match] = $match_display;
      }
    }
  }
  drupal_json_output(array_slice($matches, 0, 10));
}

/**
 * Builds the form for one section.
 */
function _sunmailer_section_form($section = NULL) {
  $form = array(
    '#type' => 'fieldset',
    '#title' => $section ? $section['name'] : t('New section'),
    '#collapsible' => $section,
    '#collapsed' => $section,
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => $section,
    '#default_value' => $section ? $section['name'] : '',
  );
  $form['view_display'] = array(
    '#type' => 'textfield',
    '#title' => t('View'),
    '#description' => t('Format is "view/display" (e.g.: archive/default)'),
    '#required' => $section,
    '#default_value' => $section ? "{$section['view']}/{$section['display']}" : '',
    '#autocomplete_path' => 'sunmailer/autocomplete/views',
  );
  $form['args'] = array(
    '#type' => 'textfield',
    '#title' => t('Arguments'),
    '#description' => t('Separate arguments with a / as though they were a URL path'),
    '#default_value' => $section ? $section['args'] : '',
  );
  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => $section ? $section['weight'] : 0,
  );
  if ($section) {
    $form['delete'] = array(
      '#type' => 'checkbox',
      '#title' => t('Delete'),
    );
  }
  else {
    $form['subscribe'] = array(
      '#type' => 'checkbox',
      '#title' => t('Subscribe'),
      '#description' => t('Add this section to subscribers\' subscriptions'),
    );
  }
  return $form;
}

/**
 * Generates a form for newsletter sections.
 */
function sunmailer_admin_sections() {
  $form = array();
  $form['#tree'] = TRUE;
  $sections = sunmailer_get_sections();
  foreach ($sections as $sid => $section) {
    $form[$sid] = _sunmailer_section_form($section);
  }
  $form['new'] = _sunmailer_section_form();
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Checks that the view exists for one section.
 */
function _sunmailer_section_validate($id, $section, $new) {
  if ($new && !trim($section['name'])) {
    return;
  }
  if (!$new && $section['delete']) {
    return;
  }
  $view_display = explode('/', $section['view_display']);
  views_get_all_views(); // http://drupal.org/node/670308
  $view = views_get_view($view_display[0]);
  if (!$view) {
    form_set_error("$id][view_display", t('View does not exist.'));
  }
  if ($view->base_table != 'node') {
    form_set_error("$id][view_display", t('View is not a node view.'));
  }
  if ($view->disabled) {
    form_set_error("$id][view_display", t('View is disabled.'));
  }
  if (!array_key_exists($view_display[1], $view->display)) {
    form_set_error("$id][view_display", t('View does not contain this display.'));
  }
}

/**
 * Validation callback for newsletter settings form.
 */
function sunmailer_admin_sections_validate($form, &$form_state) {
  $values = $form_state['values'];
  $sections = sunmailer_get_sections();
  foreach ($sections as $sid => $v) {
    _sunmailer_section_validate($sid, $values[$sid], FALSE);
  }
  _sunmailer_section_validate('new', $values['new'], TRUE);
}

/**
 * Submission callback for newsletter settings form.
 */
function sunmailer_admin_sections_submit($form, &$form_state) {
  $values = $form_state['values'];
  $sections = sunmailer_get_sections();
  foreach ($sections as $sid => $v) {
    $section = $values[$sid];
    // Delete section, unsubscribe users from it.
    if ($section['delete']) {
      db_delete('sunmailer_user_sections')->condition('sid', $sid)->execute();
      db_delete('sunmailer_sections')->condition('sid', $sid)->execute();
    }
    // Update section.
    else {
      $view_display = explode('/', $section['view_display']);
      db_update('sunmailer_sections')
        ->fields(array(
          'name' => check_plain(trim($section['name'])),
          'view' => $view_display[0],
          'display' => $view_display[1],
          'args' => $section['args'],
          'weight' => $section['weight'],
        ))
        ->condition('sid', $sid)
        ->execute();
    }
  }
  // Check for a new section.
  $section = $values['new'];
  $name = check_plain(trim($section['name']));
  if ($name) {
    $view_display = explode('/', $section['view_display']);
    $record = new stdClass();
    $record->name = $name;
    $record->view = $view_display[0];
    $record->display = $view_display[1];
    $record->args = $section['args'];
    $record->weight = $section['weight'];
    drupal_write_record('sunmailer_sections', $record);
    // Subscribe users to the new section.
    if ($section['subscribe']) {
      $result = db_query("SELECT DISTINCT(uid) FROM {sunmailer_user_sections}");
    }
    else {
      $result = db_query("SELECT uid FROM {sunmailer_user_sections} GROUP BY uid HAVING COUNT(sid) = :count", array(':count' => count($sections)));
    }
    $query = db_insert('sunmailer_user_sections')->fields(array('uid', 'sid'));
    foreach ($result as $row) {
      $query->values(array(
        'uid' => $row->uid,
        'sid' => $record->sid,
      ));
    }
    $query->execute();
  }
}

/**
 * Builds the form for one format.
 */
function _sunmailer_format_form($xid, $format) {
  // Generate the title and XSL name for the format.
  $new = $xid == 'new';
  $type = $format['html'] ? 'HTML' : 'text';
  if ($new) {
    $title = t('New format');
  }
  else {
    $xsl_name = db_query("SELECT filename FROM {file} WHERE fid = :fid", array(':fid' => $format['fid']))->fetchField();
    $title = $format['name'];
    $title .= $format['custom'] ? '' : t(' (default @type format)', array('@type' => $type));
  }
  // Build the form.
  $form = array(
    '#type' => 'fieldset',
    '#title' => $title,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => !$new,
    '#default_value' => $format['name'],
  );
  $form['html'] = array(
    '#type' => 'radios',
    '#title' => t('Type'),
    '#options' => array(
      FALSE => t('Text'),
      TRUE => t('HTML'),
    ),
    '#default_value' => $new ? TRUE : $format['html'],
    '#disabled' => !$new,
  );
  $form['file'] = array(
    '#type' => 'file',
    '#title' => t('XSL'),
    '#description' => t('XSL file used to build this format from XML content'),
  );
  if (!$new) {
    $form['xsl_name'] = array(
      '#type' => 'markup',
      '#prefix' => '<p>',
      '#value' => '<strong>' . t('Current XSL File:') . "</strong> $xsl_name",
      '#suffix' => '</p>',
    );
  }
  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => $new ? 0 : $format['weight'],
  );
  if (!$new) {
    if (!$format['custom']) {
      if (($format['fid'] != variable_get('sunmailer_default_html_fid', 0)) && ($format['fid'] != variable_get('sunmailer_default_text_fid', 0))) {
        $form['reset'] = array(
          '#type' => 'checkbox',
          '#title' => t('Reset XSL file'),
        );
      }
    }
    else {
      $form['delete'] = array(
        '#type' => 'checkbox',
        '#title' => t('Delete'),
        '#description' => t('Users with this format will be switched to the default @type format', array('@type' => $type)),
      );
    }
  }
  return $form;
}

/**
 * Generates a form for newsletter formats.
 */
function sunmailer_admin_formats() {
  $form = array();
  $form['#tree'] = TRUE;
  $form['#attributes'] = array('enctype' => 'multipart/form-data');
  $formats = sunmailer_get_formats();
  foreach ($formats as $xid => $format) {
    $form[$xid] = _sunmailer_format_form($xid, $format);
  }
  $form['new'] = _sunmailer_format_form('new', array());
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Validation callback for XSLT file.
 */
function sunmailer_is_xsl_file(&$file) {
  $doc = new DOMDocument();
  return $doc->load($file->filepath) ? array() : array(t('This is not a valid XSL file.'));
}

/**
 * Validation callback for newsletter formats form.
 */
function sunmailer_admin_formats_validate($form, &$form_state) {
  $values = $form_state['values'];
  $validators = array(
    'sunmailer_is_xsl_file' => array(),
  );
  // Validate formats.
  $formats = sunmailer_get_formats();
  foreach ($formats as $xid => $v) {
    if (!$format['delete']) {
      $form_state['values'][$xid]['xsl'] = file_save_upload($xid, $validators, file_directory_path());
    }
  }
  if (trim($values['new']['name'])) {
    $xsl = file_save_upload('new', $validators, file_directory_path());
    if (!$xsl) {
      form_set_error('new][file', t('XSL file required'));
    }
    $form_state['values']['new']['xsl'] = $xsl;
  }
}

/**
 * Submission callback for newsletter formats form.
 */
function sunmailer_admin_formats_submit($form, &$form_state) {
  $values = $form_state['values'];
  $formats = sunmailer_get_formats();
  foreach ($formats as $xid => $v) {
    $format = $values[$xid];
    // Find the file for the current XSL file.
    $fid = db_query("SELECT fid FROM {sunmailer_formats} WHERE xid = :xid", array(':xid' => $xid))->fetchField();
    $path = db_query("SELECT uri FROM {file} WHERE fid = :fid", array(':fid' => $fid))->fetchField();
    // Delete custom format.
    if ($format['delete']) {
      db_delete('file')->condition('fid', $fid)->execute();
      file_delete($path);
      db_update('sunmailer_user_format')
        ->fields(array('xid' => $formats[$xid]['html'] ? SUNMAILER_HTML_FORMAT : SUNMAILER_TEXT_FORMAT))
        ->condition('xid', $xid)
        ->execute();
      db_delete('sunmailer_formats')->condition('xid', $xid)->execute();
    }
    // Update custom format.
    else {
      if ($format['reset']) {
        db_delete('file')->condition('fid', $fid)->execute();
        file_delete($path);
        $fid = variable_get(($xid == SUNMAILER_HTML_FORMAT) ? 'sunmailer_default_html_fid' : 'sunmailer_default_text_fid', 0);
      }
      else {
        $file = $format['xsl'];
        if ($file) {
          if (($fid != variable_get('sunmailer_default_html_fid', 0)) && ($fid != variable_get('sunmailer_default_text_fid', 0))) {
            db_delete('file')->condition('fid', $fid)->execute();
            file_delete($path);
          }
          file_set_status($file, FILE_STATUS_PERMANENT);
          $fid = $file->fid;
        }
      }
      db_update('sunmailer_formats')
        ->fields(array(
          'name' => check_plain(trim($format['name'])),
          'fid' => $fid,
          'weight' => $format['weight'],
        ))
        ->condition('xid', $xid)
        ->execute();
    }
  }
  // Check for a new format.
  $format = $values['new'];
  $name = check_plain(trim($format['name']));
  if ($name) {
    $file = $format['xsl'];
    file_set_status($file, FILE_STATUS_PERMANENT);
    $new_format = new stdClass();
    $new_format->name = $name;
    $new_format->html = $format['html'];
    $new_format->fid = $file->fid;
    $new_format->weight = $format['weight'];
    drupal_write_record('sunmailer_formats', $new_format);
  }
  // Update default format.
  variable_set('sunmailer_default_format', (int)$values['default_format']);
}

function sunmailer_admin_subscribe() {
  $formats = sunmailer_get_formats();
  $format_options = array(0 => 'Not subscribed');
  foreach ($formats as $xid => $format) {
    $format_options[$xid] = $format['name'];
  }
  $form = array();
  $form['subscribe_register'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display subscription form on user registration page'),
    '#default_value' => variable_get('sunmailer_subscribe_register', TRUE),
  );
  $form['default_format'] = array(
    '#type' => 'radios',
    '#title' => t('Default format'),
    '#description' => t('This format will initially be selected on the user registration page'),
    '#options' => $format_options,
    '#default_value' => variable_get('sunmailer_default_format', SUNMAILER_HTML_FORMAT),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

function sunmailer_admin_subscribe_submit($form, &$form_state) {
  $values = $form_state['values'];
  variable_set('sunmailer_subscribe_register', (bool)$values['subscribe_register']);
  variable_set('sunmailer_default_format', (int)$values['default_format']);
}
